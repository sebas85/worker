import config from 'config';
import Debug from 'debug';
import { Kafka } from 'kafkajs';
import Redis from 'ioredis';
import fs from 'fs';

import ProcessNewLoraUplink from "./tasks/lora/ProcessNewLoraUplink";

(async () => {

    const context = {
        config
    };
    context.debug = new Debug('worker');
    const { debug } = context;

    debug('Connect to Redis');
    const redisOptions = {
        host: config.get('redis.host'),
        port: config.get('redis.port'),
        db: config.get('redis.db')
    };
    if(config.has('redis.username')) redisOptions.username = config.get('redis.username');
    if(config.has('redis.password')) redisOptions.password = config.get('redis.password');
    if(process?.env?.NODE_ENV === 'production') redisOptions.tls = { ca: fs.readFileSync("/opt/app/ibmredis.pem") };
    context.redis = new Redis(redisOptions);

    debug('Connect to Kafka');
    let kafkaOptions = {
        clientId: process?.env?.HOSTNAME || 'worker',
        brokers: config.get('kafka.brokers'),
        retry: {
            initialRetryTime: 1000,
            factor: 0.5,
            multiplier: 3,
            retries: 10
        }
    };
    if(process.env.NODE_ENV === 'production') {
        kafkaOptions = {
            ...kafkaOptions,
            ssl: true,
            sasl: {
                mechanism: 'plain',
                username: config.get('kafka.user'),
                password: config.get('kafka.password')
            }
        }
    }
    const kafka = new Kafka(kafkaOptions);

    debug('Connecting producer');
    const producer = kafka.producer()
    await producer.connect();

    debug('Connecting consumer');
    const consumer = kafka.consumer({ groupId: config.get('kafka.groupId') })
    await consumer.connect();
    debug('Connected');

    context.kafka = { producer };

    debug('Subscribe to topics');
    await consumer.subscribe({ topic: config.get('kafka.topics.ttiIngest'), fromBeginning: false });

    await consumer.run({
        eachMessage: async ({topic, partition, message}) => {
            switch (topic) {
                case config.get('kafka.topics.ttiIngest'):
                    ProcessNewLoraUplink({ message, context });
                    break;
            }
        }
    });

})();