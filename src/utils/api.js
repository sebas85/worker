import axios from 'axios';
import config from 'config';

const client = axios.create({
    baseURL: config.get('api.host'),
    headers: {
        'x-api-key': '9c25ee7a24604c4eb9d1c1e88bc5e9e0'
    },
    json: true
});

export const getLoraDeviceInfo = ({ applicationId, devEUI }) => (
    client.request({
        method: 'GET',
        url: '/devices/info',
        params: {
            source: 'tti',
            applicationId,
            devEUI
        }
    }).then(r => r.data)
)
