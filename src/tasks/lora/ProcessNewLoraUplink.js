import {getLoraDeviceInfo} from "../../utils/api";


export default async function ProcessNewLoraUplink({ context, message })  {

    const { config, debug, kafka, redis } = context;
    debug('New message in Kafka TTI ingest');

    const value = message.value.toString();
    const data = JSON.parse(value);
    debug(data);
    const { applicationId, devEUI, payload, time } = data;

    if(applicationId && devEUI) {

        try {
            debug('Getting data from Redis cache');
            const cachedData = await redis.get(`${applicationId}-${devEUI}`);
            debug(cachedData);

            let apiDeviceDetails;
            if (!cachedData) {
                debug('No cached data. Fetching from API');
                apiDeviceDetails = await getLoraDeviceInfo({
                    applicationId,
                    devEUI
                });
                debug(apiDeviceDetails)
            }

            const deviceDetails = cachedData ? JSON.parse(cachedData) : apiDeviceDetails;
            debug(deviceDetails)

            if(deviceDetails) {
                const messages = [];
                for(const [ key, value ] of Object.entries(payload)) {
                    const msg = {
                        time,
                        deviceId: deviceDetails.device.deviceId,
                        deviceName: deviceDetails.device.name,
                        deviceType: deviceDetails.device.type,
                        variable: key,
                        value
                    };

                    if(deviceDetails.building) {
                        msg.buildingId = deviceDetails.building.buildingId;
                        msg.buildingName = deviceDetails.building.name;
                        msg.buildingType = deviceDetails.building.type;
                    }

                    if(deviceDetails.section) {
                        msg.sectionId = deviceDetails.section.sectionId;
                        msg.sectionName = deviceDetails.section.name;
                        msg.sectionType = deviceDetails.section.type;
                    }

                    messages.push({
                        value: JSON.stringify(msg)
                    });
                }

                debug('Sending lora data messages to kafka');
                debug(messages);
                kafka.producer.send({
                    topic: config.get('kafka.topics.loraData'),
                    messages
                });

            }

        } catch(err) {
            console.error(err);
        }

    }

}