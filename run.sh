#!/usr/bin/env bash

if [ "$NODE_ENV" = "development" ]
then
  yarn
  npm run dev
fi

if [ "$NODE_ENV" = "production" ]
then
  npm start
fi
